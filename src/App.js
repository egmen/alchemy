import React, { Component } from 'react';
import './App.css';
import _ from 'lodash';

const FIRST_LEVEL_ITEMS = 'Воздух-Огонь-Вода-Земля'.split('-')
const Context = React.createContext();

const Element = (props) => {
  const {name} = props
  const className = _.includes(FIRST_LEVEL_ITEMS, name)
    ? 'first-level-element'
    : ''

  return <Context.Consumer>
    {({build}) => {
      const buildList = build[name]
      return ( 
        <span
          className={className}
          title={buildList && buildList.join('\n')}
        >
          {name}
        </span>
      )
    }}
  </Context.Consumer>
}

class List extends Component {
  render() {
    const {name} = this.props

    return (
      <Context.Consumer>
        {({list, structure, setSumResult}) => {
          const strItem = structure[name] || {}

          return (
            <div>
              <ul>
                {list
                  .filter((item) => item !== name) // Фильтр самого элемента
                  .filter((item) => strItem[item] !== false) // Фильтр несуществующей связи
                  .map((item) => {
                    const hasResult = strItem[item]
                    if (hasResult) {
                      return <li key={item}>
                        <Element name={name} /> + <Element name={item} /> = {' '}{hasResult}
                        <List name={hasResult} />
                      </li>
                    }
                    return <li key={item} className='no-result'>
                      <Element name={name} /> + <Element name={item} /> = {' '}
                      <span
                        onClick={() => {
                          const newElement = window.prompt('Название элемента')
                          setSumResult(name, item, newElement)
                        }}
                      >
                        Ввести
                      </span>
                      {' / '}
                      <span
                        onClick={() => setSumResult(name, item, false)}
                      >
                        Нет результата
                      </span>
                    </li>
                  })
                }
              </ul>
            </div>
          )
        }}
      </Context.Consumer>
    )
  }
}

const howToBuild = (name, structure) => {
  const result = []
  for (let add1 in structure) {
    for (let add2 in structure) {
      const value = structure[add1][add2]
      if (value === name) {
        if (!_.includes(FIRST_LEVEL_ITEMS, add2)) {
          result.push(...howToBuild(add2, structure))
        }
        if (!_.includes(FIRST_LEVEL_ITEMS, add1)) {
          result.push(...howToBuild(add1, structure))
        }
        result.push(`${add2} + ${add1} = ${value}`)
        return (result)
      }
    }
  }
  return null
}

class App extends Component {
  constructor (props) {
    super(props)

    this.state = {
      structure: {}
    }
    const structure = window.localStorage.getItem('structure')
    if (structure) {
      this.state.structure = JSON.parse(structure)
    }
  }

  setSumResult = (add1, add2, name) => {
    const {structure} = this.state
    const path1 = `${add1}.${add2}`
    const path2 = `${add2}.${add1}`
    _.set(structure, path1, name)
    _.set(structure, path2, name)
    this.setState({structure})
    window.localStorage.setItem('structure', JSON.stringify(structure))
  }

  get additionalElemets () {
    const {structure} = this.state
    const structureList = _.chain(structure)
      .map(_.toArray)
      .value()
    const union = _.union(...structureList).filter(item => item)
    return union
  }

  get list () {
    return _.union(FIRST_LEVEL_ITEMS, this.additionalElemets)
  }

  get build () {
    return this.additionalElemets.reduce((prev, curr) => {
      prev[curr] = howToBuild(curr, this.state.structure)
      return prev
    }, {})
  }

  render() {
    const {structure} = this.state

    return (
      <Context.Provider
        value={{
          list: this.list,
          build: this.build,
          structure,
          setSumResult: this.setSumResult
        }}
      >
        <div className="App">
          <button
            onClick={() => {
              window.localStorage.setItem('structureBackup', JSON.stringify(structure))
            }}
          >
            Сохранить
          </button>
          {FIRST_LEVEL_ITEMS.map((item) => {
            return <List
              key={item}
              name={item}
            />
          })}
        </div>
      </Context.Provider>
    );
  }
}

export default App;
